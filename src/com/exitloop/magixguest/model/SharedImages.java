package com.exitloop.magixguest.model;

import android.graphics.Bitmap;

public class SharedImages {
	public int id;
	public String filename;
	public Bitmap bm;
	public String message;
}
