package com.exitloop.magixguest;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.os.AsyncTask;
import android.util.Log;

public class DownloadImagesTask extends AsyncTask<String, Void, Bitmap> {

	Canvas drawCanvas = null;
	Paint canvasPaint = null;
	
	public void setDrawCanvas(Canvas cv, Paint cp)
	{
		drawCanvas = cv;
		canvasPaint = cp;
	}
	
	@Override
	protected Bitmap doInBackground(String... urls) {
	    return download_Image(urls[0]);
	}
	
	@Override
	protected void onPostExecute(Bitmap result) {
		Bitmap workingBitmap = Bitmap.createBitmap(result);
		Bitmap mutableBitmap = workingBitmap.copy(Bitmap.Config.ARGB_8888, true);
		drawCanvas.drawBitmap(mutableBitmap, 0, 0, canvasPaint);
	}	
	
	private Bitmap download_Image(String url) {
		Bitmap bm = null;
	    try {
	        URL aURL = new URL(url);
	        URLConnection conn = aURL.openConnection();
	        conn.connect();
	        InputStream is = conn.getInputStream();
	        BufferedInputStream bis = new BufferedInputStream(is);
	        bm = BitmapFactory.decodeStream(bis);
	        bis.close();
	        is.close();
	    } catch (IOException e) {
	        Log.e("Hub","Error getting the image from server : " + e.getMessage().toString());
	    } 
	    
	    return bm;
	}

}
